Przykład wykorzystania Kafka w Spring
Na podstawie artykułu:

- [Event-Driven Architectures with Kafka and Java Spring-Boot"](https://itnext.io/event-driven-architectures-with-kafka-and-java-spring-boot-6ded048e86f3)
- [Kafka Docker Uruchom wielu brokerów Kafka w Docker](http://wurstmeister.github.io/kafka-docker/)


#### Uruchomienie serwera Kafka
Uruchom klaster:
`docker-compose up -d`

Dodaj więcej brokerów:
`docker-compose scale kafka=3`

Zatrzymaj klaster:
`docker-compose stop`

#### Uruchomienie aplikacji:
`spring-kafka-app\consumer>java -jar ./target/consumer-0.0.1.jar
`
`spring-kafka-app\producer>java -jar ./target/producer-0.0.1.jar
`
